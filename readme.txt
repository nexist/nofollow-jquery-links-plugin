=== NoFollow jQuery Links ===
Contributors: nexist
Donate link: https://nex.ist/
Tags: seo, jquery, internal linking, crawl, link juice
Requires at least: 4.6
Tested up to: 5.2.2
Stable tag: trunk
Requires PHP: 5.2.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A simple TinyMCE Plugin to add a js link solution for linking pages together in order to stop search engines crawlers going through those pages.

== Description ==

A simple TinyMCE Plugin to add a java-script link solution for linking pages together in order to stop search engines crawlers go through those pages and keep them just in the related pages of the same topic.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the new JS buttons in the TinyMCE toolbar to add jQuery links to any text.


== Frequently Asked Questions ==

= Will the jquery links have a nofollow attribute or similar for the search crawlers? =

No. These links are expected to be completely hidden from the SE crawlers so that no link juice is leaked on to the pages/posts referred to from these links.

== Screenshots ==

1. Highlighed in red are the additional buttons on the TinyMCE editor showing the options to add, add (with target _blank) and to remove the jslink.

== Changelog ==

= 1.0 =
* First stable version.

== Upgrade Notice ==

= 1.0 =
First public release.